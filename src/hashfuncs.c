#include "hashfuncs.h"



HashValue hashpjw(const char *key, void *user)
{
    const char *p;
    HashValue h = 0, g;
    for (p = key; *p; p = p + 1) {
	h = (h << 4) + (*p);
	if ((g = h & 0xf0000000) != 0) {
	    h = h ^ (g >> 24);
	    h = h ^ g;
	}
    }
    return h;
}

HashValue hashdjb2(const char *key, void *user)
{
/* 
djb2 a simple hash function

From: http://www.cse.yorku.ca/~oz/hash.html 

If you just want to have a good hash function, and cannot wait, djb2
is one of the best string hash functions i know. it has excellent
distribution and speed on many different sets of keys and table
sizes. you are not likely to do better with one of the "well known"
functions such as PJW, K&R[1], etc. Also see tpop pp. 126 for graphing
hash functions.

djb2 

this algorithm (k=33) was first reported by dan bernstein many years
ago in comp.lang.c. another version of this algorithm (now favored by
bernstein) uses xor: hash(i) = hash(i - 1) * 33 ^ str[i]; the magic of
number 33 (why it works better than many other constants, prime or
not) has never been adequately explained.

*/
    HashValue hash = 5381;
    int c;

    while ((c = *key++))
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    return hash;
}

/* from wikipedia */
HashValue hashjoaat(const char *key, void *user)
 {
     HashValue hash = 0;
     char k;
     
     while ((k = *key++)) {
         hash += k;
         hash += (hash << 10);
         hash ^= (hash >> 6);
     }
     hash += (hash << 3);
     hash ^= (hash >> 11);
     hash += (hash << 15);
     return hash;
 }


HashValue hashlookup3(const char *key, void *user)
{
    uint32_t salt = 0xbeeeeeef;
    return((HashValue) hashlittle(key, strlen(key), salt));
}

#if 0
int hashcode(char *key, int size, int alg)
{
    int h = 0;                  /* -Wall */
    uint32_t salt, mask;

    /* XXX: FIXME: need a safer way to get log2 of size */
    mask = hashmask((int) log2(size));

    switch (alg) {
    case HASH_PJW:
        h = hashpjw(key) & mask;
        break;
    case HASH_LOOKUP3:
        salt = 0xbeeeeeef;
        h = (int) (hashlittle(key, strlen(key), salt)
                   & mask);
        break;
    default:
        error("unknown hash type");
    }
    return h;
}

SEXP h3(SEXP keys, SEXP log2size)
{
    uint32_t h, salt = 0xbeeeeeef;
    const char *s;
    int i;
    SEXP ans;

    uint32_t mask = hashmask(INTEGER(log2size)[0]);

    PROTECT(ans = allocVector(INTSXP, length(keys)));
    for (i = 0; i < length(keys); ++i) {
        s = CHAR(STRING_ELT(keys, i));
        h = hashlittle(s, strlen(s), salt);
        h = (h & mask);
        INTEGER(ans)[i] = (int) h;
    }
    UNPROTECT(1);
    return ans;
}
#endif
