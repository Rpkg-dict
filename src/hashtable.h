#include "Rtypes.h"

/* Macros for hash table access */

#define HASHSIZE(x)	     LENGTH(x)
#define HASHPRI(x)	     TRUELENGTH(x)
#define HASHTABLEGROWTHRATE  1.2
#define HASHMINSIZE	     29
#define SET_HASHSIZE(x,v)    SETLENGTH(x,v)
#define SET_HASHPRI(x,v)     SET_TRUELENGTH(x,v)
#define IS_HASHED(x)	     (HASHTAB(x) != R_NilValue)

#define HASHCOUNT(x)         TRUELENGTH(x) /* number of items in the table */
#define SET_HASHCOUNT(x,v)     SET_TRUELENGTH(x,v)

#define NEXT_CHAIN_EL(e) (CDR(e))
#define SET_NEXT_CHAIN_EL(e1, e2) (SETCDR(e1, e2))
