#include <stdint.h>     /* defines uint32_t etc */
#include <sys/param.h>  /* attempt to define endianness */
#ifdef linux
# include <endian.h>    /* attempt to define endianness */
#endif

uint32_t hashword(
const uint32_t *k,                 /* the key, an array of uint32_t values */
size_t          length,           /* the length of the key, in uint32_ts */
uint32_t        initval);         /* the previous hash, or an arbitrary value */
uint32_t hashlittle( const void *key, size_t length, uint32_t initval);

#define hashsize(n) ((uint32_t)1<<(n))
#define hashmask(n) (hashsize(n)-1)
#define rot(x,k) (((x)<<(k)) | ((x)>>(32-(k))))
