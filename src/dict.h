#ifndef DICT_H
#define DICT_H

#include "Rtypes.h"
#include "hashfuncs.h"

/* .Call API for R-level access */
SEXP dict_new_dict(SEXP, SEXP, SEXP);
SEXP dict_copy_dict(SEXP);
SEXP dict_get_info(SEXP);
SEXP dict_get_item(SEXP, SEXP);
SEXP dict_getm(SEXP, SEXP, SEXP);
SEXP dict_set_item(SEXP, SEXP, SEXP);
SEXP dict_del_item(SEXP, SEXP);
SEXP dict_num_items(SEXP);
SEXP dict_get_keys(SEXP, SEXP);
SEXP dict_to_list(SEXP);

SEXP dict_list_hashfuns(void);
SEXP dict_get_hash_codes(SEXP, SEXP, SEXP);
SEXP dict_xp_tag_address(SEXP);

/* internal */
int  is_dict_full(SEXP);
SEXP resize_dict(SEXP, int);
SEXP copy_dict(SEXP, SEXP);
SEXP chain_dict_get(SEXP, const char *);
SEXP chain_dict_set(SEXP, const char *, SEXP);
SEXP chain_dict_del(SEXP, const char *);
SEXP chain_dict_keys(SEXP);

typedef struct dict_obj {
    int type;
    SEXP (*get_item)(SEXP, const char *);
    SEXP (*set_item)(SEXP, const char *, SEXP);
    SEXP (*del_item)(SEXP, const char *);
    SEXP (*get_keys)(SEXP);
    SEXP (*get_values)(SEXP);
    /* TODO: rehash? */
    SEXP (*get_stats)(SEXP);
    char* name;
} DictObj;

static const DictObj DictTypes[] = {
    {0,
     &chain_dict_get,
     &chain_dict_set,
     &chain_dict_del,
     &chain_dict_keys,
     NULL, NULL, "chain"},
/* SOON:    {1, "open"}, */
    {-1, NULL, NULL, NULL, NULL, NULL, NULL, NULL}
};

#define CHAIN_DICT 0
#define OADDR_DICT 1

#define DICT_TYPE_IDX 0
#define HASHFUN_IDX   1
#define COUNT_IDX      2

#define DICT_DETAIL(t) INTEGER((VECTOR_ELT(t, 0)))
#define DICT_TABLE(t) (VECTOR_ELT(t, 1))

#define DICT_SIZE(x) LENGTH(DICT_TABLE(x))
#define SET_DICT_SIZE(x, v) SETLENGTH(DICT_TABLE(x), v)

#define DICT_ALG(t) DICT_DETAIL(t)[HASHFUN_IDX]
#define DICT_TYPE(t) DICT_DETAIL(t)[DICT_TYPE_IDX]

#define DICT_COUNT(x) DICT_DETAIL(x)[COUNT_IDX]
#define SET_DICT_COUNT(x, v) (DICT_DETAIL(x)[COUNT_IDX] = v)
#define GET_HASHFUN(t) (Dict_HashFunctions[DICT_ALG(t)].hash)
#define GET_HASHNAME(t) (Dict_HashFunctions[DICT_ALG(t)].name)
#define GET_DICT_TYPE_NAME(t) (DictTypes[DICT_TYPE(t)].name)
#define GET_DICTGET(t) (DictTypes[DICT_TYPE(t)].get_item)
#define GET_DICTSET(t) (DictTypes[DICT_TYPE(t)].set_item)
#define GET_DICTDEL(t) (DictTypes[DICT_TYPE(t)].del_item)
#define GET_DICTKEYS(t) (DictTypes[DICT_TYPE(t)].get_keys)


#endif  /* DICT_H */
