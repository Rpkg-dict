#include "Rtypes.h"
#include "hashtable.h"
#include "lookup3.h"
#include <stdint.h>     /* defines uint32_t etc */

typedef unsigned long HashValue;
typedef HashValue (*HashFunc)(const char *, void *); /* ptr to hash function */

typedef struct hash_func_entry {
    HashFunc hash;
    char* name;
} HashFuncEntry;

#define hashsize(n) ((uint32_t)1<<(n))
#define hashmask(n) (hashsize(n)-1)

HashValue hashpjw(const char *key, void *);
HashValue hashdjb2(const char *key, void *);
HashValue hashjoaat(const char *key, void *);
HashValue hashlookup3(const char *key, void *);


static const HashFuncEntry Dict_HashFunctions[] = {
    {&hashpjw, "PJW"},
    {&hashdjb2, "DJB2"},
    {&hashjoaat, "JOAAT"},
    {&hashlookup3, "LOOKUP3"},
    {NULL, NULL}
};

#if 0
HashValue hashcode(char *key, int size, int alg);
#endif
