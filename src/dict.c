#include "dict.h"

/* .Call registration */
static const R_CallMethodDef R_CallDef[] =
{
    {"dict_new_dict", (DL_FUNC)&dict_new_dict, 3},
    {"dict_copy_dict", (DL_FUNC)&dict_copy_dict, 1},
    {"dict_get_info", (DL_FUNC)&dict_get_info, 1},
    {"dict_get_item", (DL_FUNC)&dict_get_item, 2},
    {"dict_getm",     (DL_FUNC)&dict_getm, 3},
    {"dict_set_item", (DL_FUNC)&dict_set_item, 3},
    {"dict_del_item", (DL_FUNC)&dict_del_item, 2},
    {"dict_get_keys", (DL_FUNC)&dict_get_keys, 2},
    {"dict_list_hashfuns", (DL_FUNC)&dict_list_hashfuns, 0},
    {"dict_get_hash_codes", (DL_FUNC)&dict_get_hash_codes, 3},
    {"dict_num_items", (DL_FUNC)&dict_num_items, 1},
    {"dict_to_list", (DL_FUNC)&dict_to_list, 1},
    {"dict_xp_tag_address", (DL_FUNC)&dict_xp_tag_address, 1},
    {NULL, NULL, 0},
};

/* number of hash functions in Dict_HashFunctions */
static int g_hash_func_count = 0;

void R_init_dict(DllInfo *info)
{
    int i;

    R_registerRoutines(info, NULL, R_CallDef, NULL, NULL);

    for (i = 0; Dict_HashFunctions[i].hash != NULL; i++); /* empty */
    g_hash_func_count = i;
}

int is_hash_func_idx(int idx)
{
    return idx >= 0 && idx < g_hash_func_count;
}

SEXP dict_list_hashfuns()
{
    SEXP ans, nms;
    int i, *code;

    PROTECT(ans = allocVector(INTSXP, g_hash_func_count));
    PROTECT(nms = allocVector(STRSXP, g_hash_func_count));
    SET_NAMES(ans, nms);
    code = INTEGER(ans);
    for (i = 0; i < g_hash_func_count; i++) {
        SET_STRING_ELT(nms, i, mkChar(Dict_HashFunctions[i].name));
        code[i] = i;
    }
    UNPROTECT(2);
    return ans;
}

SEXP dict_get_hash_codes(SEXP keys, SEXP table_size, SEXP alg)
{
    SEXP ans;
    int i, size, *pans, mask;

    if (!isInteger(alg) || !is_hash_func_idx(INTEGER(alg)[0]))
        error("'alg' must be an integer code matching a hash function");

    if (!isInteger(table_size))
        error("'table_size' must be an integer");

    if (!isString(keys))
        error("'keys' must be character");

    HashFunc hash = Dict_HashFunctions[INTEGER(alg)[0]].hash;

    size = INTEGER(table_size)[0];
    mask = size - 1;
    PROTECT(ans = allocVector(INTSXP, length(keys)));
    pans = INTEGER(ans);
    for (i = 0; i < length(keys); i++) {
        pans[i] = (*hash)(CHAR(STRING_ELT(keys, i)), NULL) & mask;
    }
    SET_NAMES(ans, NAMED(keys) ? duplicate(keys) : keys);
    UNPROTECT(1);
    return ans;
}

static SEXP hashStats(SEXP table)
{
    SEXP chain, ans, chain_counts, nms;
    int i, count;

    PROTECT(ans = allocVector(VECSXP, 3));
    PROTECT(nms = allocVector(STRSXP, 3));
    SET_STRING_ELT(nms, 0, mkChar("size")); /* total size of hashtable */
    SET_STRING_ELT(nms, 1, mkChar("items")); /* number of items in the table */
    SET_STRING_ELT(nms, 2, mkChar("counts"));  /* length of each chain */
    SET_NAMES(ans, nms);
    UNPROTECT(1);

    SET_VECTOR_ELT(ans, 0, ScalarInteger(length(table)));
    SET_VECTOR_ELT(ans, 1, ScalarInteger(HASHCOUNT(table)));

    PROTECT(chain_counts = allocVector(INTSXP, length(table)));
    for (i = 0; i < length(table); i++) {
        chain = VECTOR_ELT(table, i);
        count = 0;
        for (; chain != R_NilValue ; chain = CDR(chain)) {
            count++;
        }
        INTEGER(chain_counts)[i] = count;
    }

    SET_VECTOR_ELT(ans, 2, chain_counts);

    UNPROTECT(2);
    return ans;
}

SEXP getHashStats(SEXP env)
{
    SEXP ans = R_NilValue;      /* -Wall */

    if (isEnvironment(env)) {
        if (IS_HASHED(env))
            ans = hashStats(HASHTAB(env));
        else
            ans = ScalarInteger(NA_INTEGER);
    } else
        error("argument must be a hashed environment");
    return ans;
}


SEXP dict_num_items(SEXP xp)
{
    SEXP dict = R_ExternalPtrTag(xp);
    return ScalarInteger(DICT_COUNT(dict));
}

static SEXP new_dict(int size, int alg, int type)
{
    SEXP details, dict, table = R_NilValue; /* -Wall */
    int *d;
    PROTECT(dict = allocVector(VECSXP, 2));
    switch (type) {
    case CHAIN_DICT:
        PROTECT(table = allocVector(VECSXP, size));
        break;
    case OADDR_DICT:
        error("dict type %d not implemented", OADDR_DICT);
        break;
    default:
        error("unknown dict type: %d", type);
    }
    PROTECT(details = allocVector(INTSXP, 4));
    d = INTEGER(details);
    d[DICT_TYPE_IDX] = type;
    d[HASHFUN_IDX] = alg;
    d[COUNT_IDX] = 0;

    SET_VECTOR_ELT(dict, 0, details);
    SET_VECTOR_ELT(dict, 1, table);

    UNPROTECT(3);
    return dict;
}

SEXP dict_new_dict(SEXP size, SEXP alg, SEXP type)
{
    SEXP table, xp;
    /* TODO: add error checking and validation of size and alg args */
    /* XXX: size must be 2^n */
    PROTECT(table = new_dict(INTEGER(size)[0], INTEGER(alg)[0],
                              INTEGER(type)[0]));
    PROTECT(xp = R_MakeExternalPtr(NULL, R_NilValue, R_NilValue));
    R_SetExternalPtrTag(xp, table);
    UNPROTECT(2);
    return xp;
}

static SEXP get_info(SEXP dict)
{
    SEXP ans, nms;
    PROTECT(ans = allocVector(VECSXP, 4));
    PROTECT(nms = allocVector(STRSXP, 4));
    SET_STRING_ELT(nms, 0, mkChar("size"));
    SET_STRING_ELT(nms, 1, mkChar("nitems"));
    SET_STRING_ELT(nms, 2, mkChar("type"));
    SET_STRING_ELT(nms, 3, mkChar("alg"));
    SET_NAMES(ans, nms);

    SET_VECTOR_ELT(ans, 0, ScalarInteger(DICT_SIZE(dict)));
    SET_VECTOR_ELT(ans, 1, ScalarInteger(DICT_COUNT(dict)));
    SET_VECTOR_ELT(ans, 2, mkString(GET_DICT_TYPE_NAME(dict)));
    SET_VECTOR_ELT(ans, 3, mkString(GET_HASHNAME(dict)));

    UNPROTECT(2);
    return ans;
}

SEXP dict_get_info(SEXP xp)
{
    SEXP dict;
    dict = R_ExternalPtrTag(xp);
    return get_info(dict);
}

static SEXP get_item(SEXP dict, const char *key)
{
    SEXP (*get_item)(SEXP, const char*) = GET_DICTGET(dict);
    return (*get_item)(dict, key);
}

SEXP dict_get_item(SEXP xp, SEXP key)
{
    SEXP dict, item;
    const char *s = NULL;

    if (isString(key))
        s = CHAR(STRING_ELT(key, 0));
    else if (TYPEOF(key) == CHARSXP)
        s = CHAR(key);
    else
        error("arg 'key' must be a character vector");
    dict = R_ExternalPtrTag(xp);
    item = get_item(dict, s);
    /* NOTE: I'm not sure this is what we want, but it seems to help
       the case when unnamed items are in the dict, get retrieved and
       then modified.  But I don't quite follow because subassign
       looks like it only checks for NAMED(obj) == 2 and here we only
       set it to 1 and so I don't yet see where it gets incremented to
       trigger duplication (but it seems to work as desired).  */
    if (!isNull(item) && NAMED(item) == 0)
        SET_NAMED(item, 1);
    return item;
}

static SEXP set_item(SEXP dict, const char *key, SEXP value)
{
    SEXP (*set_item)(SEXP, const char*, SEXP) = GET_DICTSET(dict);
    return (*set_item)(dict, key, NAMED(value) ? duplicate(value) : value);
}

SEXP dict_set_item(SEXP xp, SEXP key, SEXP value)
{
    SEXP dict;
    const char *s;

    if (!isString(key))
        error("arg 'key' must be a character vector");
    s = CHAR(STRING_ELT(key, 0));
    dict = R_ExternalPtrTag(xp);
    if (is_dict_full(dict)) {
        dict = resize_dict(dict, DICT_SIZE(dict) * 2);
        R_SetExternalPtrTag(xp, dict);
#ifdef DEBUG_DICT_RESIZE
        Rprintf("resized to %d\n", DICT_SIZE(dict));
#endif
    }
    return set_item(dict, s, value);
}

static SEXP del_item(SEXP dict, const char *key)
{
   SEXP (*del_item)(SEXP, const char*) = GET_DICTDEL(dict);
   return (*del_item)(dict, key);
}

SEXP dict_del_item(SEXP xp, SEXP key)
{
    SEXP dict;
    const char *s;

    if (!isString(key))
        error("arg 'key' must be a character vector");
    s = CHAR(STRING_ELT(key, 0));
    dict = R_ExternalPtrTag(xp);
    return del_item(dict, s);
}

static SEXP get_keys(SEXP dict, int sort)
{
    SEXP keys;
    SEXP (*get_keys)(SEXP) = GET_DICTKEYS(dict);
    PROTECT(keys = (*get_keys)(dict));
    if (sort)
        error("sorry, R decided not to make sortVector available");
    UNPROTECT(1);
    return keys;

}

SEXP dict_get_keys(SEXP xp, SEXP sort)
{
    int do_sort = 0;
    SEXP dict = R_ExternalPtrTag(xp);
    if (!isLogical(sort) || (do_sort = LOGICAL(sort)[0]) == NA_LOGICAL)
        error("'sort' must be TRUE or FALSE");
    return get_keys(dict, do_sort);
}

static SEXP to_list(SEXP dict)
{
    SEXP ans, keys, val;
    int i;

    PROTECT(keys = get_keys(dict, 0));
    PROTECT(ans = allocVector(VECSXP, length(keys)));
    for (i = 0; i < length(keys); i++) {
        val = get_item(dict, CHAR(STRING_ELT(keys, i)));
        SET_VECTOR_ELT(ans, i, NAMED(val) ? duplicate(val) : val);
    }
    SET_NAMES(ans, keys);
    UNPROTECT(2);
    return ans;
}

SEXP dict_to_list(SEXP xp)
{
    SEXP dict = R_ExternalPtrTag(xp);
    return to_list(dict);
}

int is_dict_full(SEXP dict)
{
    /* resize based on number of items compared to table size */
    if (DICT_COUNT(dict) > (DICT_SIZE(dict) * 10))
        return 1;
    else
        return 0;
}

SEXP resize_dict(SEXP dict, int size)
{
    SEXP newd;

    PROTECT(newd = new_dict(size, DICT_ALG(dict), DICT_TYPE(dict)));
    UNPROTECT(1);
    return copy_dict(dict, newd);
}

SEXP copy_dict(SEXP srcdict, SEXP destdict)
{
    SEXP keys;
    int i;
    const char *key;
    PROTECT(keys = get_keys(srcdict, 0));
    for (i = 0; i < length(keys); i++) {
        key = CHAR(STRING_ELT(keys, i));
        set_item(destdict, key, get_item(srcdict, key));
    }
    UNPROTECT(1);
    return destdict;
}

SEXP dict_copy_dict(SEXP xp)
{
    SEXP newxp;
    SEXP dict = R_ExternalPtrTag(xp);
    PROTECT(newxp = R_MakeExternalPtr(NULL, R_NilValue, R_NilValue));
    R_SetExternalPtrTag(newxp, resize_dict(dict, DICT_SIZE(dict)));
    UNPROTECT(1);
    return newxp;
}

SEXP dict_xp_tag_address(SEXP xp)
{
    SEXP ans;
    char buf[100];
    int over;
    over = snprintf(buf, 100, "%p", (void *) R_ExternalPtrTag(xp));
    ans = mkString(buf);
    return ans;
}

SEXP dict_getm(SEXP xp, SEXP keys, SEXP ifnotfound)
{
    SEXP ans, nms, dict, val, curkey;
    int i, j;

    PROTECT(nms = allocVector(STRSXP, length(keys)));
    PROTECT(ans = allocVector(VECSXP, length(keys)));

    dict = R_ExternalPtrTag(xp);

    for (i = 0, j = 0; i < length(keys); i++) {
        curkey = STRING_ELT(keys, i);
        val = get_item(dict, CHAR(curkey));
        /* FIXME: what about putting NULL as the value?  How to distinguish? */
        if (val == R_NilValue) {
            if (ifnotfound == R_NilValue)
                continue;
            /* needed? */
            val = duplicate(ifnotfound);
        }
        SET_VECTOR_ELT(ans, j, val);
        SET_STRING_ELT(nms, j++, curkey);
    }
    if (j < i) {                /* then resize */
        UNPROTECT(1);
        SET_LENGTH(ans, j);     /* XXX: this can alloc */
        PROTECT(ans);
        SET_LENGTH(nms, j);
    }
    SET_NAMES(ans, nms);
    UNPROTECT(2);
    return ans;
}

#if 0


static SEXP ht_table_rehash(SEXP table, int new_size, int alg)
{
    SEXP new_table, chain, new_chain, val;
    int counter, new_hashcode;

    if (TYPEOF(table) != VECSXP)
        error("ht_table_rehash: argument 'table' not of type VECSXP");

    /* Allocate the new hash table */
    new_table = ht_new_table(ScalarInteger(new_size));
    PROTECT(new_table);

    for (counter = 0; counter < length(table); counter++) {
        chain = VECTOR_ELT(table, counter);
        while (!isNull(chain)) {
            val = CAR(chain);
            new_hashcode = hashcode(CHAR(val), new_size, alg);
            new_chain = VECTOR_ELT(new_table, new_hashcode);
            SET_VECTOR_ELT(new_table, new_hashcode,  CONS(val,  new_chain));
            chain = NEXT_CHAIN_EL(chain);
        }
    }
    SET_HASHCOUNT(new_table, HASHCOUNT(table));

#ifdef DEBUG_HT_RESIZE
    char *status = HASHPRI(new_table) > HASHPRI(table) ? " OK " : "FAIL";
    Rprintf("Resized: size %d => %d\tpri %d => %d\t%s\n",
            HASHSIZE(table), HASHSIZE(new_table),
            HASHPRI(table), HASHPRI(new_table), status);
#endif
    UNPROTECT(1);
    return new_table;
}

SEXP ht_hashtable_getm(SEXP xp, SEXP keys, SEXP ifnotfound, SEXP hash)
{
    SEXP ans, table, val;
    int i, j;

    PROTECT(ans = allocVector(VECSXP, length(keys)));
    table = R_ExternalPtrTag(xp);

    for (i = 0, j = 0; i < length(keys); i++) {
        val = ht_table_get(table, ScalarString(STRING_ELT(keys, i)), hash);
        /* FIXME: what about putting NULL as the value?  How to distinguish? */
        if (val == R_NilValue) {
            if (ifnotfound == R_NilValue)
                continue;
            /* needed? */
            val = duplicate(ifnotfound);
        }
        SET_VECTOR_ELT(ans, j++, val);
    }
    if (j < i)                  /* then resize */
        SET_LENGTH(ans, j);
    UNPROTECT(1);
    return ans;
}


int ht_table_check_size1(SEXP table)
{
    /* resize based on number of items compared to table size */
    if ( (HASHSIZE(table) * 10) < HASHCOUNT(table))
        return 1;
    else
        return 0;
}

SEXP ht_hashtable_aslist(SEXP xp)
{
    SEXP ans, nms;
    SEXP table, chain;
    int i, j;

    table = R_ExternalPtrTag(xp);
    PROTECT(ans = allocVector(VECSXP, HASHCOUNT(table)));
    PROTECT(nms = allocVector(STRSXP, HASHCOUNT(table)));

    for (i = 0, j = 0; i < HASHSIZE(table); i++) {
        chain = VECTOR_ELT(table, i);
        for (; chain != R_NilValue; chain = CDR(chain)) {
            SET_VECTOR_ELT(ans, j, duplicate(CAR(chain)));
            SET_STRING_ELT(nms, j, duplicate(TAG(chain)));
            j++;
        }
    }

    setAttrib(ans, R_NamesSymbol, nms);
    UNPROTECT(2);
    return ans;
}

SEXP ht_hashtable_keys(SEXP xp)
{
    SEXP nms;
    SEXP table, chain;
    int i, j;

    table = R_ExternalPtrTag(xp);
    PROTECT(nms = allocVector(STRSXP, HASHCOUNT(table)));

    for (i = 0, j = 0; i < HASHSIZE(table); i++) {
        chain = VECTOR_ELT(table, i);
        for (; chain != R_NilValue; chain = CDR(chain)) {
            SET_STRING_ELT(nms, j, duplicate(TAG(chain)));
            j++;
        }
    }

    UNPROTECT(1);
    return nms;
}




#endif
