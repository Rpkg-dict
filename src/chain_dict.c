#include "dict.h"

SEXP chain_dict_get(SEXP dict, const char *key)
{
    SEXP table, chain, val;
    HashValue h;
    HashFunc hash;
    int idx;

    table = DICT_TABLE(dict);
    hash = GET_HASHFUN(dict);
    h = (*hash)(key, NULL);
    idx = (DICT_SIZE(dict) - 1) & h;
#ifdef DEBUG_HASHING
    Rprintf("%s => %d => %d\n", key, h, idx);
#endif

    /* Grab the chain from the hashtable */
    chain = VECTOR_ELT(table, idx);
    /* Retrieve the value from the chain */
    for (; chain != R_NilValue ; chain = CDR(chain)) {
        val = CAR(chain);
	if (strcmp(CHAR(TAG(chain)), key) == 0)
            return val;
    }
    /* If not found */
    return R_NilValue;
}

SEXP chain_dict_set(SEXP dict, const char *key, SEXP value)
{
    /* XXX: modifies table SEXP table in place !!!!!! */
    SEXP table, chain, chain_tmp, val;
    HashValue h;
    HashFunc hash;
    int idx;

    table = DICT_TABLE(dict);
    hash = GET_HASHFUN(dict);
    h = (*hash)(key, NULL);
    idx = (DICT_SIZE(dict) - 1) & h;
#ifdef DEBUG_HASHING
    Rprintf("%s => %d => %d\n", key, h, idx);
#endif
    /* Grab the chain from the hashtable */
    chain = chain_tmp = VECTOR_ELT(table, idx);
    /* Search for the value in the chain */
    for (; !isNull(chain_tmp); chain_tmp = CDR(chain_tmp)) {
        val = CAR(chain_tmp);
	if (strcmp(CHAR(TAG(chain_tmp)), key) == 0) {
            /* set to new value */
            SETCAR(chain_tmp, value);
	    return R_NilValue;
	}
    }

    /* Add the value at head of chain */
    SET_DICT_COUNT(dict, DICT_COUNT(dict) + 1);
    PROTECT(chain = CONS(value, chain));
    /* this indicates that we should pass in the SEXP, not the char* */
/*     SET_TAG(chain, STRING_ELT(key, 0)); */
    SET_TAG(chain, mkChar(key));
    SET_VECTOR_ELT(table, idx, chain);
    UNPROTECT(1);
    return R_NilValue;
}

SEXP chain_dict_del(SEXP dict, const char *key)
{
    SEXP table, chain, prev;
    HashValue h;
    HashFunc hash;
    int idx, found = 0;

    table = DICT_TABLE(dict);
    hash = GET_HASHFUN(dict);
    h = (*hash)(key, NULL);
    idx = (DICT_SIZE(dict) - 1) & h;
#ifdef DEBUG_HASHING
    Rprintf("%s => %d => %d\n", key, h, idx);
#endif

    chain = VECTOR_ELT(table, idx);
    prev = R_NilValue;
    for (; chain != R_NilValue; prev = chain, chain = CDR(chain)) {
	if (strcmp(CHAR(TAG(chain)), key) == 0) {
            if (prev != R_NilValue)
                SETCDR(prev, CDR(chain));
            else
                SET_VECTOR_ELT(table, idx, CDR(chain));
            SET_DICT_COUNT(dict, DICT_COUNT(dict) - 1);
            found = 1;
            break;
        }
    }
    return ScalarLogical(found);
}

SEXP chain_dict_keys(SEXP dict)
{
    SEXP nms;
    SEXP table, chain, key;
    int i, j;

    table = DICT_TABLE(dict);
    PROTECT(nms = allocVector(STRSXP, DICT_COUNT(dict)));

    for (i = 0, j = 0; i < DICT_SIZE(dict); i++) {
        chain = VECTOR_ELT(table, i);
        for (; chain != R_NilValue; chain = CDR(chain)) {
            key = TAG(chain);
            SET_STRING_ELT(nms, j, NAMED(key) ? duplicate(key) : key);
            j++;
        }
    }

    UNPROTECT(1);
    return nms;
}
