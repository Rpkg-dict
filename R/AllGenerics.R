setGeneric("del", function(d, k) standardGeneric("del"))

setGeneric("keys", signature="d",
           function(d, sort=FALSE) standardGeneric("keys"))
