\name{listHashFunctions}
\alias{listHashFunctions}

\title{List available hashing functions in the dict package}
\description{
  This function returns a named integer vector with an entry for each
  hashing function that is available for use with \code{Dict}
  objects.  Each hashing function is identified by an integer code and
  a short name.
}
\usage{
listHashFunctions()
}

\details{
  \describe{
    \item{PJW}{This is the hash function used in the R sources.  It
      comes from the second edition of the "Dragon Book" by Aho, Ullman
      and Sethi.}

    \item{DJB2}{This hash function comes from
      \url{http://www.cse.yorku.ca/~oz/hash.html}.  It is attributed to
      Dan Bernstein and was posted in comp.lang.c.}

    \item{JOAAT}{This hash function comes from wikipedia.  See
      \url{http://en.wikipedia.org/wiki/Hash_table}}

    \item{LOOKUP3}{This hash function is by Bob Jenkins and has been
      placed in the public domain.  More inormation is available here:
      \url{http://burtleburtle.net/bob/hash/doobs.html}}
  }

  Additional hash functions can be added by editing
  \code{src/hashfuncs.h} and adding an entry to the
  \code{Dict_HashFunctions} array.  All hash functions need to take a
  single argument of type \code{char*} and return a \code{HashValue}
  (see \code{src/hashfuncs.h} for details).
}
\value{
  An integer vector.  The names are the short names of the available
  hashing algorithms.  The values are the corresponding integer codes
  that identify the hashing functions.  These values can be passed to
  \code{newDict} using the \code{hash.alg} argument to select a
  particular hashing function.
}

\author{Seth Falcon}
\seealso{ \code{\link{newDict}}}
\examples{
listHashFunctions()
}

\keyword{ }

