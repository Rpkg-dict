\name{hashCodes}
\alias{hashCodes}

\title{Obtain Raw Hash Codes}
\description{
  The \code{hashCodes} function is intended to allow direct comparison
  of hashing functions.  It returns the hashcode modulo the specified
  table size.
}
\usage{
hashCodes(keys, size, hash.alg = 0L)
}

\arguments{
  \item{keys}{A character vector of keys.}
  \item{size}{The size of the hypothetical table as an integer.}
  \item{hash.alg}{An integer indicating which hashing function to
  use.  See \code{listHashFunctions}.}
}

\value{
  A named integer vector.  The names match the provided \code{keys}
  and the values are the integer index into a table of size
  \code{size}.
}

\author{Seth Falcon}

\examples{
algs <- listHashFunctions()
keys <- letters

table(hashCodes(keys, 16L, algs[1]))
table(hashCodes(keys, 16L, algs[2]))
table(hashCodes(keys, 16L, algs[3]))
}


\keyword{ }

